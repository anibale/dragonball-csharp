﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Clases.Personajes
{
    public class Bullet : MultiImageSprite
    {
        int speed = 15; //velocidad de disparo


        static SoundEffect Sound;

        public Bullet(Point location)
        {
            // TODO: Mi Lasser
            if (Sound == null)
                Sound = Game1.CurrentGame.Content.
                    Load<SoundEffect>("Sonidos/Disparo");

            Location = location;

            imagenes.Add(0,
                Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/greenLaser"));
        }

        bool SoundPlayed = false;


        public override void Update(GameTime gameTime)
        {

            if (SoundPlayed == false)
            {
                SoundPlayed = true;
                Sound.Play();
            }

            int x = Frame.X;
            int y = Frame.Y;
            x += speed;

            if (x > Game1.CurrentGame.GraphicsDevice.Viewport.Width * 2)
            {
                Game1.CurrentGame.NewSprites.Add(this);
            }

            foreach (var item in Game1.CurrentGame.Sprites)
            {
                if (item is Cell)
                {
                    Cell enemy = item as Cell;


                    if (Frame.Intersects(enemy.Frame))
                    {
                        int healt;
                        healt = enemy.healtenemy;
                        enemy.healtenemy = healt - 20;

                        if (enemy.healtenemy <= 0)
                        {
                            Game1.CurrentGame.NewSprites.Add(enemy);
                            Game1.CurrentGame.NewSprites.Add(this);
                            Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                            Game1.Score = Game1.Score + 50;

                        }
                        else
                        {
                            Game1.CurrentGame.NewSprites.Add(this);
                            Game1.Score = Game1.Score + 10;

                        }

                    }
                }

                    else
                        {
                            if (item is MasterFreezer)
                            { 
                            MasterFreezer Master = item as MasterFreezer;


                            if (Frame.Intersects(Master.Frame))
                            {
                                int healt;
                                healt = Master.healtenemy;
                                Master.healtenemy = healt - 20;

                                if (Master.healtenemy <= 0)
                                {
                                    Game1.CurrentGame.NewSprites.Add(Master);
                                    Game1.CurrentGame.NewSprites.Add(this);
                                    Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                                    Game1.Score = Game1.Score + 10000;
                                    Game1.FinGame = true;    
                                }
                                else
                                {
                                    Game1.CurrentGame.NewSprites.Add(this);
                                    Game1.Score = Game1.Score + 100;
                                }

                            }


                            }


                    }

                Frame = new Rectangle(x, y, 105, 60);
            }


        }

    }
}


