﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Clases.Personajes
{
    public class Bullet2 : MultiImageSprite
    {
        int speed = 10;
        static SoundEffect Sound;


        public Bullet2(Point location)
        {

            if (Sound == null)
                Sound = Game1.CurrentGame.Content.
                    Load<SoundEffect>("Disparar2");
            // TODO: Complete member initialization
            Location = location;

            imagenes.Add(0,
                Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/Blue_laser"));
        }


        bool SoundPlayed = false;

        public override void Update(GameTime gameTime)
        {
            if (SoundPlayed == false)
            {
                SoundPlayed = true;
                Sound.Play();
            }

            int x = Frame.X;
            int y = Frame.Y;
            x += speed;
            if (x > Game1.CurrentGame.GraphicsDevice.Viewport.Width * 2)
            {
                Game1.CurrentGame.NewSprites.Add(this);
            }

            Frame = new Rectangle(x, y, 100, 100);

            foreach (var item in Game1.CurrentGame.Sprites)
            {
                if (item is Cell)
                {
                    Cell enemy = item as Cell;
                    if (Frame.Intersects(enemy.Frame))
                    {
                        int healt;
                        healt = enemy.healtenemy;
                        enemy.healtenemy = healt - 40;
                        if (enemy.healtenemy <= 0)
                        {
                            Game1.CurrentGame.NewSprites.Add(enemy);
                            Game1.CurrentGame.NewSprites.Add(this);
                            Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                            Game1.Score = Game1.Score + 50;
                       
                        }
                        else
                        {
                            Game1.CurrentGame.NewSprites.Add(this);
                            Game1.CurrentGame.NewSprites.Add(new Explotion(Location));
                            Game1.Score = Game1.Score + 25;
                        }

                    }

                }
            }

        }
    }
}
