﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Sprites
{
    public abstract class SpriteMultiFrame : Sprite
    {
        protected Texture2D imagen;
        
        protected Dictionary<object,Rectangle> frames;
        protected int currentFrame;

        public SpriteMultiFrame()
        {
            frames = new Dictionary<object, Rectangle>();           
        }
        public override void Draw(GameTime gameTime)
        {
            //Game1.CurrentGame.spriteBatch.Draw(imagen, Frame, frames[currentFrame], Color.White);
            Game1.CurrentGame.spriteBatch.Draw(imagen, new Vector2(Location.X, Location.Y),
                                                frames[currentFrame], Color.White);
        }
    }
}
