﻿using Game.Clases.Juego;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


////Genera Rectangulo donde se incrustan las imagenes y se ubica el lugar donde
///esta la Vegeta en este acaso
namespace Game.Clases.Sprites
{

      public abstract class Sprite : Updateable
      {

          public Rectangle Frame { get; protected set; }
          public Rectangle Frame1 { get; protected set; }
          public Rectangle Frame2 { get; protected set; }
          public static SpriteFont MainFont { get; private set; }
          public static SpriteFont BigFont { get; private set; }

          public Sprite()
          { 
            MainFont = Game1.CurrentGame.Content.Load<SpriteFont>("Fuentes/Stats");
            BigFont = Game1.CurrentGame.Content.Load<SpriteFont>("Fuentes/BigFont");
          
          }
          
          
          
          public Point Location
      {
             get { return Frame.Location; }
              set { Frame = new Rectangle(value, Frame.Size); }
          }

          public Point Location2
          {
              get { return Frame1.Location; }
              set { Frame1 = new Rectangle(value, Frame1.Size); }
          }

          protected virtual void ValidateLocation(ref int x, ref int y)
          {
              if (x < 0)
                  x = Game1.CurrentGame.GraphicsDevice.Viewport
                  .Width;
              else if (x > Game1.CurrentGame.GraphicsDevice.Viewport
                  .Width)
                  x = -50;
              if (y < 0)
                  y = Game1.CurrentGame.GraphicsDevice.Viewport
                  .Height;
              else if (y > Game1.CurrentGame.GraphicsDevice.Viewport
                  .Height)
                  y = -50;
          }

          
          public abstract void Draw(GameTime gameTime);
          
      
         
      } 
   
}
