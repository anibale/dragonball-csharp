﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Juego
{
    public class Fondo : MultiImageSprite
    {
        int speed = - 1;
        public Fondo()
        {
            int r2;

            imagenes.Add(0, Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/screen"));
            Frame = new Rectangle(0, 0, Game1.CurrentGame.GraphicsDevice.DisplayMode.Width,
                                       Game1.CurrentGame.GraphicsDevice.DisplayMode.Height);

            
            r2 = Frame.X + Game1.CurrentGame.GraphicsDevice.DisplayMode.Width;

            imagenes.Add(1, Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/screen2"));
            Frame1 = new Rectangle(r2, 0, Game1.CurrentGame.GraphicsDevice.DisplayMode.Width,
                                       Game1.CurrentGame.GraphicsDevice.DisplayMode.Height);
        }

        public override void Update(GameTime gameTime)
        {
            
            int x = Frame.X;
            int y = Frame.Y;
            int x1 = Frame1.X;
            int y1 = Frame1.Y;

            x += speed;
            x1 += speed;
            Location = new Point(x, y);
            Location2 = new Point(x1, y1);

            if (Location.X == (- Game1.CurrentGame.GraphicsDevice.DisplayMode.Width))
            {
                Location = new Point(x = 0, y);
            }
            if (Location2.X == 0)
            {
                Location2 = new Point(x = Game1.CurrentGame.GraphicsDevice.DisplayMode.Width,y);
            
            }
                       
        }
            
    }
}
