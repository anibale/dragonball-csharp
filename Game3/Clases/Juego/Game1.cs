﻿using Game.Clases.Personajes;
using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
using Game.Clases.Juego;
using Microsoft.Xna.Framework.Media;
using System;
using System.IO;

namespace Game
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {

        public static bool FinalPartida = false;
        static int score = 0;
        public int pos = 0;
        
        public static int life = 3;
        
        public static int Score
        {
            get { return score; }
            set { score = value; }
        }
        
        
        static int Life
        {
            get { return life; }
            set { life = value; }

        }


        public static bool FinGame = false;
        public SpriteFont MainFont;
        public SpriteFont BigFont;
        public GraphicsDeviceManager graphics { get; private set; }
        public SpriteBatch spriteBatch { get; private set; }
        public static Game1 CurrentGame { get; private set; }
        public List<Sprite> NewSprites { get; private set; }
       
        internal List<Updateable> Sprites { get; set; }
        public Matrix SpriteScale { get; private set; }

        MenuFinal MenuFinal;
        BtnIniciar botonjugar;
        static Song Musica;
        Texture2D FondoMp;
        Texture2D FondoFin;
        public Fondo fondo;
        public Vegeta avion;
        TimeSpan Tiempo;
        public static GameState _state;

        public void PasarPuntaje(int puntaje)
        {
            int counter = 0;
            string line;
            List<string> total = new List<string>();
            bool agregar = false;
            
            
            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Leer archivo dnde se ecnuentran los puntajes  
            StreamReader file = new StreamReader("puntaje.txt");
            
            while ((line = file.ReadLine()) != null)
            {
               
                if (Convert.ToInt32(line) >= score)
                {
                    if (counter < 10 )
                    {
                        if (Convert.ToInt32(line) != score)
                        {
                            total.Add(line);
                            counter++;
                        }
                        else
                        {
                            total.Add(Convert.ToString(score));
                            agregar = true;
                            counter++;
                            pos = counter;
                        
                        }
                    }
                    
                }
                else

                {
                    if (agregar == false)
                    {
                        total.Add(Convert.ToString(score));
                        agregar = true;
                        counter++;
                        pos = counter;
                        if (counter == 1)
                        {
                            total.Add(line);
                            counter++;
                        }
                    }
                    else
                    {
                       if (counter < 10)
                        {
                            total.Add(line);
                            counter++;
                        }
                    }
                    
                } 
            }

            file.Close();
            if (agregar == true)
            {
                StreamWriter filew = new StreamWriter("puntaje.txt");//agregar el puntaje al archivo
                total.ForEach(filew.WriteLine);
                filew.Close();   
            }
        }

        public enum GameState
        {
            MainMenu,
            Playing,
            Final
        }

        GameState CurrentState = GameState.MainMenu;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this); 
            //graphics.ToggleFullScreen(); //Poner pantalla completa
            Content.RootDirectory = "Content";

            this.IsMouseVisible = true;

            CurrentGame = this;
            NewSprites = new List<Sprite>();
            Sprites = new List<Updateable>();

            if (Musica == null)
            {
                Musica = this.Content.Load<Song>("Musica");

            }
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }


        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //prueba de menu principal

            FondoMp = this.Content.Load<Texture2D>("imagenes/fondo");
            FondoFin = this.Content.Load<Texture2D>("imagenes/fondo-dragon");
            MainFont = this.Content.Load<SpriteFont>("Fuentes/Stats");
            BigFont = this.Content.Load<SpriteFont>("Fuentes/BigFont");

            botonjugar = new BtnIniciar();
            Sprites.Add(botonjugar);
            //pantallas escalable - prueba
            float screenscale =
                (float)graphics.GraphicsDevice.Viewport.Width / 800f;
            // Create the scale transform for Draw. 
            // Do not scale the sprite depth (Z=1).
            SpriteScale = Matrix.CreateScale(screenscale, screenscale, 1);
            // TODO: use this.Content to load your game content here
        }
 
        void UpdateMainMenu(GameTime gameTime)
        {

            botonjugar.Update(gameTime);

            if (botonjugar.enter)
            {
                NewSprites.Add(botonjugar);
                _state = GameState.Playing;

                fondo = new Fondo();
                Sprites.Add(fondo);
                Sprites.Add(new EnemyFactory());
                avion = new Vegeta();
                Sprites.Add(avion);
                
            }
            
        }


        void DrawMainMenu(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(FondoMp, new Rectangle(0, 0, 800, 480), Color.White);
            foreach (var item in Sprites)
            {
                if (item is Sprite)
                    ((Sprite)item).Draw(gameTime);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
        
        void UpdateGameplay(GameTime gameTime)
        {
            
            if ( FinGame == true)
            {
                NewSprites.Add(fondo);
                _state = GameState.Final;
                PasarPuntaje(score);


               foreach (var item in Game1.CurrentGame.Sprites)
                {
                    if (item is Bullet)
                    {
                        Bullet BT = item as Bullet;
                        NewSprites.Add(BT);
                    }

                    if (item is Bullet2)
                    {
                        Bullet2 BT2 = item as Bullet2;
                        NewSprites.Add(BT2);
                    }
 
                   if (item is Vegeta)
                    {
                        Vegeta nave = item as Vegeta;
                        NewSprites.Add(nave);
                    
                    }
                   if (item is Cell)
                    {
                        Cell enemy = item as Cell;
                        NewSprites.Add(enemy);
                    }
                    if (item is Explotion)
                    {
                        Explotion exp = item as Explotion;
                        NewSprites.Add(exp);
                    }

                    if (item is BulletEnemy)
                    {
                        BulletEnemy bte = item as BulletEnemy;
                        NewSprites.Add(bte);
                    
                    }
                    if (item is MasterFreezer)
                    {
                        MasterFreezer enemym = item as MasterFreezer;
                        NewSprites.Add(enemym);
                        
                    }
               }
                
                
            }
            
            if (gameTime.TotalGameTime > Tiempo + Musica.Duration || Tiempo == (new TimeSpan(0, 0, 0)))
            {
                Tiempo = gameTime.TotalGameTime;
                MediaPlayer.Play(Musica);
            }


            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            foreach (var item in Sprites)
            {
                item.Update(gameTime);


            }

            foreach (var item in NewSprites)
            {
                if (Sprites.Contains(item))
                    Sprites.Remove(item);
                else
                    Sprites.Add(item);
            }
            
            NewSprites.Clear();
            base.Update(gameTime);
        }

        void DrawGameplay(GameTime gameTime)
        {
            spriteBatch.Begin();

            foreach (var item in Sprites)
            {
                if (item is Sprite)
                    ((Sprite)item).Draw(gameTime);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }


        void UpdateFinal(GameTime deltaTime)
        {
           
        }

        void DrawFinal(GameTime gameTime)
        {
            
            spriteBatch.Begin();
            
            spriteBatch.Draw(FondoFin,new Rectangle(0, 0, 800, 480), Color.Green);
            spriteBatch.DrawString(MainFont, ("TU PUNTAJE  " + Game1.Score.ToString()), new Vector2(200, 240), Color.Yellow);

            if (pos != 0)
            {
                //spriteBatch.DrawString(MainFont, ("TU PUNTAJE FUE " + Game1.Score.ToString()), new Vector2(200, 240), Color.Yellow);
                //spriteBatch.DrawString(MainFont, ("Estas en la posicion " + pos.ToString()) + " de los mejores puntajes", new Vector2(200, 280), Color.Red);

            }
            else
            {
                //spriteBatch.DrawString(MainFont, ("TU PUNTAJE FUE " + Game1.Score.ToString()), new Vector2(200, 240), Color.Yellow);
                //spriteBatch.DrawString(MainFont, "No ingresaste a la lista de los mejores puntajes", new Vector2(200, 280), Color.Red);
            }   


            foreach (var item in Sprites)
            {
                if (item is Sprite)
                    ((Sprite)item).Draw(gameTime);
            }
            spriteBatch.End();
            base.Draw(gameTime);

        }

        protected override void Update(GameTime gameTime) 
        {
            base.Update(gameTime);
            switch (_state)
            {
                case GameState.MainMenu:
                    UpdateMainMenu(gameTime);
                    break;
                case GameState.Playing:
                    UpdateGameplay(gameTime);
                    break;
                case GameState.Final:
                    UpdateFinal(gameTime);
                    break;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime deltaTime) //dibujar menu
        {
            base.Draw(deltaTime);
            switch (_state)
            {
                case GameState.MainMenu: //inicio
                    DrawMainMenu(deltaTime);
                    break;
                case GameState.Playing: //en juego
                    DrawGameplay(deltaTime);
                    break;
                case GameState.Final: //pantalla final
                    DrawFinal(deltaTime);
                    break;
            }
        }

    }
}