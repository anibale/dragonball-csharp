﻿using Game.Clases.Personajes;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Juego
{
    public class EnemyFactory : Updateable
    {
        int enemyCount;
        bool Master = false;
        public static bool FinGame = false;
        public override void Update(GameTime gameTime)
        {

            if (rand.Next(1000) < 3 && Game1.Score < 1000 && FinGame == false)
            {
                enemyCount++;
                Game1.CurrentGame.NewSprites.Add(new Cell(enemyCount));
            }

            if (Game1.Score >= 1000 && Master == false && FinGame == false)
            {
                Master = true;
                Game1.CurrentGame.NewSprites.Add(new MasterFreezer());
            }
        }
    }
}
