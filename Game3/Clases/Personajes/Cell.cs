﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Personajes
{
    public class Cell : MultiImageSprite
    {
        int speed = -5;
        
        int HealtEnemy = 100;
            
        public int healtenemy
        {
            get { return HealtEnemy; }
            set { HealtEnemy = value; }
        }
        
        
        /// <summary>
        /// constructor pregunta de examen
        /// </summary>
        
        public Cell( int cantidad)
        {
            cantidad = cantidad % 2;
            imagenes.Add(0, Game1.CurrentGame.Content.Load<Texture2D>
                    ("Imagenes/cell"));//pirateEnemy
            Frame = new Rectangle(Game1.CurrentGame.GraphicsDevice.Viewport.Width - 100,
                                  rand.Next(Game1.CurrentGame.GraphicsDevice.Viewport.Height - 500, Game1.CurrentGame.GraphicsDevice.Viewport.Height - 100), 150, 150);            
                
        }


        public override void Update(GameTime gameTime)
        {            
                int x = Frame.X;
                int y = Frame.Y;
                
                
                x += speed;
                ValidateLocation(ref x, ref y);
                Location = new Point(x, y);



                if (x > Game1.CurrentGame.GraphicsDevice.Viewport.Width * 2)
                {
                    Game1.CurrentGame.NewSprites.Add(this);
                }

                           
                if (Game.Clases.Juego.Updateable.rand.Next(10,1000) > 980)
                {
                    
                   Game1.CurrentGame.NewSprites.Add(new BulletEnemy(Location));
                }
                
            }


        protected override void ValidateLocation(ref int x, ref int y)
        {
            if (x < -140)
            {
                Game1.CurrentGame.NewSprites.Add(this);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (HealtEnemy > 20 )
            {
                Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                    ( HealtEnemy.ToString()), new Vector2(Frame.X, Frame.Y) , Color.Yellow);
                base.Draw(gameTime);
            }

            else

            {
                Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                    (HealtEnemy.ToString()), new Vector2(Frame.X, Frame.Y), Color.Red);
                base.Draw(gameTime);
            
            
            }
            

        }
    }
 }
