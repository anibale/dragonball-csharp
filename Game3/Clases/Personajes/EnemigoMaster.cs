﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Personajes
{
    class MasterFreezer : MultiImageSprite
    {
        int speed = -2;
        TimeSpan lastbullet;
        TimeSpan lastbullet2;
        int HealtEnemy = 500;
        
        bool CambiarPosicion = true;
        
        public int healtenemy
        {
            get { return HealtEnemy; }
            set { HealtEnemy = value; }
        }
        
        /// <summary>
        /// constructor pregunta de examen
        /// </summary>
        public MasterFreezer()
        {

            imagenes.Add(0, Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/master-enemy"));
            Frame = new Rectangle(Game1.CurrentGame.GraphicsDevice.Viewport.Width - 100,
                                      Game1.CurrentGame.GraphicsDevice.Viewport.Height - 400, 200, 300); //400, 300, 200        
        }

        public override void Update(GameTime gameTime)
        {

            int x = Frame.X;
            int y = Frame.Y;

            if (CambiarPosicion == true)
            {
                x += speed;
                ValidateLocation(ref x, ref y);
                Location = new Point(x, y);
                Location2 = new Point(x, 230);

            } 

            if (gameTime.TotalGameTime.Subtract(lastbullet).Milliseconds
                >= 500 )
              {
                    
                   Game1.CurrentGame.NewSprites.Add(new BulletEnemy(Location));
                   lastbullet = gameTime.TotalGameTime;    
              }

            if  (Game.Clases.Juego.Updateable.rand.Next(10,1000) > 950)
            {

                Game1.CurrentGame.NewSprites.Add(new BulletEnemy(Location2));
                lastbullet2 = gameTime.TotalGameTime;
            }

        }
        
        protected override void ValidateLocation(ref int x, ref int y)
        {
            if (x == 500)
            {
                CambiarPosicion = false;
            }
        }
        
        public override void Draw(GameTime gameTime)
        {
            if (HealtEnemy > 20 )
            {
                Game1.CurrentGame.spriteBatch.DrawString(MainFont,
                    ( HealtEnemy.ToString()), new Vector2(Frame.X, Frame.Y) , Color.Purple);
                base.Draw(gameTime);
            }

            else

            {
                Game1.CurrentGame.spriteBatch.DrawString(BigFont,
                    (HealtEnemy.ToString()), new Vector2(Frame.X, Frame.Y), Color.Red);
                base.Draw(gameTime);
            }
 
        
        }
    
    }
}
