﻿using Game.Clases.Sprites;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Clases.Personajes
{
    public abstract class LaserEnemy : MultiImageSprite
    {
        int speed = -5;
    
        public LaserEnemy()
        {
                imagenes.Add(0, Game1.CurrentGame.Content.Load<Texture2D>("Imagenes/greenLaser"));
            
        }
    }
}
